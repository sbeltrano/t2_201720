package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.vo.VOStop;
import model.vo.VOStop;

public class RingListTest {

	private IList<VOStop> listaRutas;
	private VOStop ruta1;
	private VOStop ruta2;
	private VOStop ruta3;
	private VOStop ruta4;

	private Node<VOStop> nodo1;
	private Node<VOStop> nodo2;
	private Node<VOStop> nodo3;
	private Node<VOStop> nodo4;
	
	
	


	public void scenario1(){
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new VOStop(1, "", "", "", "", "", "", "", "", null);
		ruta2 = new VOStop(2, "", "", "", "", "", "", "", "", null);
		ruta3 = new VOStop(3, "", "", "", "", "", "", "", "", null);
		ruta4 = new VOStop(4, "", "", "", "", "", "", "", "", null);
		
		nodo1 = new Node<VOStop>(ruta1);
		nodo2 = new Node<VOStop>(ruta2);
		nodo3 = new Node<VOStop>(ruta3);
		nodo4 = new Node<VOStop>(ruta4);
		
		listaRutas.add(nodo1);
		listaRutas.add(nodo2);
		listaRutas.add(nodo3);
		listaRutas.add(nodo4);
	
	}
	
	
	public void agregarInicio(){
		scenario1();
		VOStop ruta5 = new VOStop(8, "", "", "", "", "", "", "", "", null);
		Node<VOStop> nodo5 = new Node<VOStop>(ruta5);
		listaRutas.add(nodo5);
		
		assertTrue(listaRutas.getSize()==5);
		
	}
	
	public void agregarAlFinal(){
		scenario1();
		VOStop ruta5 = new VOStop(8, "", "", "", "", "", "", "", "", null);
		Node<VOStop> nodo5 = new Node<VOStop>(ruta5);
		listaRutas.addAtEnd(nodo5);
		
		assertTrue(listaRutas.getSize()==5);
	}
	
	public void agregarALaPosicion(){
		scenario1();
		VOStop ruta5 = new VOStop(8, "", "", "", "", "", "", "", "", null);
		Node<VOStop> nodo5 = new Node<VOStop>(ruta5);
		listaRutas.addAtK(nodo5, 2);
		
		assertTrue(listaRutas.getSize()==5);
	}
	



	@Test
	public void test() {
		agregarALaPosicion();
		agregarAlFinal();
		agregarAlFinal();
	}
}
