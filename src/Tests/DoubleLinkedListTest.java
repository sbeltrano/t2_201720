package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.vo.VORoute;

public class DoubleLinkedListTest {

	private IList<VORoute> listaRutas;
	private VORoute ruta1;
	private VORoute ruta2;
	private VORoute ruta3;
	private VORoute ruta4;

	private Node<VORoute> nodo1;
	private Node<VORoute> nodo2;
	private Node<VORoute> nodo3;
	private Node<VORoute> nodo4;
	
	


	
	
	public void scenario1(){
		listaRutas = new DoubleLinkedList<>();
		
		ruta1 = new VORoute(1, "", "", "", "", "", "", "", "");
		ruta2 = new VORoute(2, "", "", "", "", "", "", "", "");
		ruta3 = new VORoute(3, "", "", "", "", "", "", "", "");
		ruta4 = new VORoute(4, "", "", "", "", "", "", "", "");
		
		nodo1 = new Node<VORoute>(ruta1);
		nodo2 = new Node<VORoute>(ruta2);
		nodo3 = new Node<VORoute>(ruta3);
		nodo4 = new Node<VORoute>(ruta4);
		
		listaRutas.add(nodo1);
		listaRutas.add(nodo2);
		listaRutas.add(nodo3);
		listaRutas.add(nodo4);
	
	}
	
	
	public void agregarInicio(){
		scenario1();
		VORoute ruta5 = new VORoute(8, "", "", "", "", "", "", "", "");
		Node<VORoute> nodo5 = new Node<VORoute>(ruta5);
		listaRutas.add(nodo5);
		
		assertTrue(listaRutas.getSize()==5);
		assertTrue(listaRutas.getElement(1).equals(nodo5));

		
	}
	
	public void agregarAlFinal(){
		scenario1();
		VORoute ruta5 = new VORoute(8, "", "", "", "", "", "", "", "");
		Node<VORoute> nodo5 = new Node<VORoute>(ruta5);
		listaRutas.addAtEnd(nodo5);
		
		assertTrue(listaRutas.getSize()==5);
		assertTrue(listaRutas.getElement(5).equals(nodo5));
	}
	
	public void agregarALaPosicion(){
		scenario1();
		VORoute ruta5 = new VORoute(8, "", "", "", "", "", "", "", "");
		Node<VORoute> nodo5 = new Node<VORoute>(ruta5);
		listaRutas.addAtK(nodo5, 2);
		
		assertTrue(listaRutas.getSize()==5);
		assertTrue(listaRutas.getElement(2).equals(nodo5));
	}
	
	public void eliminar(){
		scenario1();
		VORoute ruta5 = new VORoute(8, "", "", "", "", "", "", "", "");
		Node<VORoute> nodo5 = new Node<VORoute>(ruta5);
		listaRutas.add(nodo5);
		listaRutas.delete(nodo5);
		
		assertTrue(listaRutas.getSize()==4);
	}
	public void eliminarEnK(){
		scenario1();
		VORoute ruta5 = new VORoute(8, "", "", "", "", "", "", "", "");
		Node<VORoute> nodo5 = new Node<VORoute>(ruta5);
		listaRutas.add(nodo5);
		listaRutas.deleteAtK(0);
		
		assertTrue(listaRutas.getSize()==4);
	}
	



	@Test
	public void test() {
		agregarALaPosicion();
		agregarAlFinal();
		agregarAlFinal();
	}

}
