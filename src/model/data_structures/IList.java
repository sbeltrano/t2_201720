package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();
	
	public void add(Node<T> n);
	
	public void addAtEnd(Node<T> n);

	public void addAtK(Node<T> n, int ind);

	public Node<T> getElement(int n);

	public Node<T> getCurrenteElement();
		
	public void delete(Node<T> n);

	public void deleteAtK(int n);

	public Node<T> next();

	public Node<T> previous();




}
