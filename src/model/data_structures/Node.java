package model.data_structures;

public class Node<T> {
	private T data;
	private Node<T> sig;
	private Node<T> ant;
	
	public Node( ){
		data = null;
		sig = null;
		ant = null;
	}
	
	public Node(T Pdata){
		data = Pdata;
		sig = null;
		ant = null;
	}
	
	public Node<T> darSig( ){
		return sig;
	}
	
	public Node<T> darAnt( ){
		return ant;
	}
	
	public T darData( ){
		return data;
	}
	
	public void cambiarSig(Node<T> Pnodo){
		sig = Pnodo;
	}
	
	public void cambiarAnt(Node<T> Pnodo){
		ant = Pnodo;
	}
	
	public void cambiarData(T Pdata){
		data = Pdata;
	}
}
