package model.data_structures;

import java.util.Iterator;

import model.vo.VOStop;

public class DoubleLinkedList<T> implements IList<T> {
	
	private Node<T> cabeza;
	private Node<T> cola;
	private Node<T> actual;
	private int size;

	public DoubleLinkedList(){
		cabeza = null;
		cola = null;
		actual = null;
		size = 0;
	}
	public void add(Node<T> n) {
		if(size == 0){
			cabeza = n;
			cabeza.cambiarAnt(null);
			cabeza.cambiarSig(cola);
			actual = n;
			size++;
		}
		else if(size == 1){
			n.cambiarSig(cabeza);
			cabeza.cambiarAnt(n);
			cola = cabeza;
			cabeza = n;
			actual = n;
		}
		else{
			n.cambiarSig(cabeza);
			cabeza.cambiarAnt(n);
			cabeza = n;
			actual = n;
			size++;
		}
	}

	public void addAtEnd(Node<T> n) {
		if(size == 0){
			cola = n;
			cabeza = n;
			cola.cambiarAnt(cabeza);
			cola.cambiarSig(null);
			cabeza.cambiarAnt(null);
			cabeza.cambiarSig(cola);
			actual =  n;
			size++;
		}
		else{
			cola.cambiarSig(n);
			n.cambiarAnt(cola);
			n.cambiarSig(null);
			cola = n;
			size++;
		}
	}
	
	public Node<T> getPreNode(int ind){
		if (ind == 0) {
			return cabeza;
		}
		Node<T> re = cabeza;
		for(int i = 0; i < ind-1; i++){
			re = re.darSig();
		}
		return re;
	}
	
	public Node<T> getPrePreNode(int ind){
		Node<T> re = cabeza;
		for(int i = 0; i < ind-2; i++){
			re = re.darSig();
		}
		return re;
	}
	
	public void addAtK(Node<T> n,int ind){
		if(size == 0){
			add(n);
		}
		if(size == 1){
			if(ind == 1){
				add(n);
			}
			else{
				addAtEnd(n);
			}
		}
		else{
			Node<T> ant = getPrePreNode(ind);
			Node<T> sig = getPreNode(ind);
			n.cambiarAnt(ant);
			n.cambiarSig(sig);
			ant.cambiarSig(n);
			sig.cambiarAnt(n);
			size++;
		}
	}

	public Node<T> getCurrenteElement() {
		return actual;
	}

	public Integer getSize() {
		return size;
	}
	
	public Node<T> next(){
		actual = actual.darSig();
		return actual;
	}
	
	public Node<T> previous(){
		actual = actual.darAnt();
		return actual;
	}
	
	public void delete(Node<T> n){
		if(n.darSig() == null && n.darAnt() == null){
			n = null;
		}
		else if(n == cabeza){
			cabeza = n.darSig();
			cabeza.cambiarAnt(null);
		}
		else if(n == cola){
			cola = n.darAnt();
			cola.cambiarSig(null);
		}
		else{
			n.darAnt().cambiarSig(n.darSig());
			n.darSig().cambiarAnt(n.darAnt());
		}
	}
	
	public void deleteAtK(int ind){
		Node<T> n = getPreNode(ind);
		delete(n);
	}
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Node<T> getElement(int n) {
		// TODO Auto-generated method stub
		return getPreNode(n);
	}

}
