package model.data_structures;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class RingList<T> implements IList<T>{
	private Node<T> cabeza;
	private Node<T> cola;
	private Node<T> actual;
	private int size;
	
	public RingList(){
		cabeza = null;
		cola = null;
		actual = null;
		size = 0; 
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	public void add(Node<T> n) {
		if(isEmpty()){
			cabeza = n;
			cola = n;
			cabeza.cambiarSig(n);
			size++;
		}
		else{
			n.cambiarSig(cabeza);
			cabeza.cambiarSig(n);
			cabeza = n;
			size++;
		}
	}
	public void addAtEnd(Node<T> n) {
		if(isEmpty()){
			cola = n;
			cabeza = n;
			cola.cambiarSig(n);
			size++;
		}
		else{
			cola.cambiarSig(n);
			n.cambiarSig(n);
			cola = n;
			size++;
		}
	}
	
	public Node<T> getPreNode(int ind){
		Node<T> re = cabeza;
		for(int i = 0; i < ind-1; i++){
			re = re.darSig();
		}
		return re;
	}
	
	public Node<T> getPrePreNode(int ind){
		Node<T> re = cabeza;
		for(int i = 0; i < ind-2; i++){
			re = re.darSig();
		}
		return re;
	}
	
	public void addAtK(Node<T> n,int ind){
		if(size == 0){
			add(n);
			cabeza = n;
			size++;
		}
		if(size == 1){
			if(ind == 1){
				add(n);
				cabeza = n;
				size++;
			}
			else{
				addAtEnd(n);
				cola = n;
				size++;
			}
		}
		else{
			Node<T> ant = getPrePreNode(ind);
			Node<T> sig = getPreNode(ind);
			n.cambiarSig(sig);
			ant.cambiarSig(n);
			size++;
		}
	}
	public Node<T> getCurrentElement() {
		return actual;
	}

	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	//@Override
	public Integer getSize() {
		return size;
	}

	//@Override
	public void delete(Node<T> n) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				if (isEmpty()) {
					return;
				}
				Node<T> nuevoPrimero = cola.darSig();
				cabeza = nuevoPrimero;
				--size;
	}

	//@Override
	public void deleteAtK(int n) {
		// TODO Auto-generated method stub
		if (isEmpty()) {
			return;
		}
		if (getSize() == 0 && n==0) {
			cabeza = null;
			cola = null;
			--size;
		}
		Node<T> aEliminar = getPreNode(n);
		if (aEliminar.equals(null)) {
			return;
		}
		else
		aEliminar.cambiarSig(null);

		--size;
	}

	//@Override
	public Node<T> next() {
		return actual.darSig();
		// TODO Auto-generated method stub
		
	}

	//@Override
	public Node<T> previous() {
		// TODO Auto-generated method stub
		//return null;
		return actual.darAnt();
	}

	public void forEach(Consumer<? super T> arg0) {
		// TODO Auto-generated method stub
		
	}

	public Spliterator<T> spliterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public Node<T> getElement(int n) {
		return getPreNode(n);
	}

	public Node<T> getCurrenteElement() {
		return actual;
	}

	
}
