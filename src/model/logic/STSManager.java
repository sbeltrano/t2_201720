package model.logic;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTimes;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class STSManager<E> implements ISTSManager {
	
	private IList<VOStop> stops;
	private IList<VOStopTimes> stopT;
	private IList<VOTrip> trips;
	private IList<VORoute> route;
	private ArrayList<Integer> numeroStops;
	private ArrayList<Integer> numeroStopT;
	private ArrayList<Integer> numeroTrips;
	private ArrayList<Integer> numeroRoutes;

	
	public STSManager(){
		route = new DoubleLinkedList<>();
		trips = new DoubleLinkedList<>();
		stopT = new DoubleLinkedList<>();
		stops = new DoubleLinkedList<>();
		numeroRoutes = new ArrayList<>();
		numeroTrips = new ArrayList<>();
		numeroStops = new ArrayList<>();
		numeroStopT = new ArrayList<>();

	}
	public void agregarParadasATrips( ){
		
		Node<VOTrip> actualT = trips.getCurrenteElement();
		while(actualT.darSig() != null)
		{
			Node<VOStopTimes> actualST = stopT.getCurrenteElement();
			while(actualST.darSig() != null)
			{
				if(actualST.darData().tripID == actualT.darData().tripID)
				{
					Node<VOStop> actualS = stops.getCurrenteElement();
					while(actualS.darSig() != null)
					{
						if(actualS.darData().stopID == actualST.darData().stopID)
						{
							actualT.darData().stops.addAtEnd(actualS);
						}
						actualS = actualS.darSig();
					}
				}
				actualST = actualST.darSig();
			}
			actualT = actualT.darSig();
		}
	}
	
	public void agregarTripsARoads( )
	{
		Node<VORoute> actualR = route.getCurrenteElement();
		while(actualR.darSig() != null)
		{
			Node<VOTrip> actualT = trips.getCurrenteElement();
			while(actualT.darSig() != null)
			{
				if(actualR.darData().routeID == actualT.darData().roadID)
				{
					actualR.darData().trips.add(actualT);
				}
				actualT = actualT.darSig();
			}
			actualR = actualR.darSig();
		}
	}

	//@Override
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( routesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String ID = partes[0];
					int routeID = Integer.parseInt(ID);
					String agencyID = partes[1];
					String routeShortName = partes[2];
					String routeLongName = partes[3];
					String routeDesc = partes[4];
					String routeType = partes[5];
					String routeURL = partes[6];
					String routeColor = partes[7];
					String routeTextColor = partes[8];
					VORoute rout=new VORoute(routeID, agencyID, routeShortName, routeLongName, routeDesc, routeType, routeURL, routeColor, routeTextColor);
					Node<VORoute> nodo = new Node(rout);
					route.add(nodo);
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
			}
		}
		//agregarTripsARoads();
		
	}

	//@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( tripsFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String sID = partes[0];
					int routeID = Integer.parseInt(sID);
					String serviceID = partes[1];
					String tID = partes[2];
					int tripID = Integer.parseInt(tID);
					String tripHeadSign = partes[3];
					String tripShortName = partes[4];
					String directionID = partes[5];
					String blockID = partes[6];
					String shapeID = partes[7];
					String wheelChair = partes[8];
					String bikesAllowed = "";
					if (partes.length > 9) {
						bikesAllowed = partes[9];
					}
					
					VOTrip trip = new VOTrip(routeID,serviceID,tripID,tripHeadSign,tripShortName,directionID,blockID,shapeID,wheelChair,bikesAllowed);
					Node<VOTrip> nodo = new Node(trip);
					trips.add(nodo);
					
					
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
			}
		}
		//agregarParadasATrips();
	}

	//@Override
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopTimesFile );
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String tID = partes[0];
					int tripID = Integer.parseInt(tID);
					String arrivalTime = partes[1];
					String departureTime = partes[2];
					String sID = partes[3];
					int stopID = Integer.parseInt(sID);
					String stopSequence = partes[4];
					String stopHeadsign = partes[5];
					String pickupType = partes[6];
					String dropOffType = partes[7];
					String shapeDistTraveled = "";
					if (partes.length > 8) {
						shapeDistTraveled = partes[8];
					}
					
					VOStopTimes stopTime = new VOStopTimes(tripID,arrivalTime,departureTime,stopID,stopSequence,stopHeadsign,pickupType,dropOffType,shapeDistTraveled);
					Node<VOStopTimes> nodo = new Node<VOStopTimes>(stopTime);
					stopT.add(nodo);
					
					linea = lector.readLine( );
				}

				linea = lector.readLine( );

				lector.close( );
			}
			catch( IOException e )
			{
			}
		}
		
	}

	//@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		File archivoE = new File( stopsFile );
		String[] ruta;
		if( archivoE.exists( ) )
		{

			try
			{
				BufferedReader lector = new BufferedReader( new FileReader( archivoE ) );

				String linea = lector.readLine( );
				linea = lector.readLine();

				while( linea != null )
				{
					String[] partes = linea.split(",");
					String ID = partes[0];
					int stopID = Integer.parseInt(ID);
					String stopCode = partes[1];
					String stopName = partes[2];
					String stopDesc = partes[3];
					String stopLat = partes[4];
					String stopLon = partes[5];
					String zoneID = partes[6];
					String stopURL = partes[7];
					String locationType = partes[8];
					String parentStation = "";
					if (partes.length > 9) {
						parentStation = partes[9];
					}
					
					VOStop stop = new VOStop(stopID,stopCode,stopName,stopDesc,stopLat,stopLon,zoneID,stopURL,locationType,parentStation);
					Node<VOStop> nodo = new Node(stop);
					stops.add(nodo);

					linea = lector.readLine( );
				}
				lector.close( );
			}
			catch( IOException e )
			{
			}
		}
		
	}

	//@Override
	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		IList<VORoute> routeAtStop = new DoubleLinkedList<>();
		Node <VOStop> actual = stops.getCurrenteElement();
		//System.out.println(stopName);		

		boolean p = false;
		while(actual.darSig()!=null && !p){
			if (actual.darData().getName().equals(stopName)) {
				//System.out.println("entro y es "+actual.darData().getName());	
				p = true;
			}
			else actual = actual.darSig();
		}
		
		Node <VOStopTimes> actualStopTimes = stopT.getCurrenteElement();
		int idStop = actual.darData().id(); 
		while (actualStopTimes.darSig()!=null) {
			if (actualStopTimes.darData().darStopId()==idStop) {
				numeroTrips.add(actualStopTimes.darData().darTripId());
				actualStopTimes = actualStopTimes.darSig();
			}
			else actualStopTimes = actualStopTimes.darSig();
		}	
		//System.out.println("el numero trips es "+numeroTrips.size());	

		Node<VOTrip> actualTrip = trips.getCurrenteElement();
		//System.out.println("Dar tripID "+actualTrip.darData().darTripId());	

		for (int i = 0; i < numeroTrips.size(); i++) {
			//System.out.println("entro a forTripId");
			while(actualTrip.darSig()!=null){
				if (actualTrip.darData().darTripId()==numeroTrips.get(i)) {
					
					//System.out.println("entro a buscarTripId");
					numeroRoutes.add(actualTrip.darData().darRoadId());
					actualTrip = actualTrip.darSig();
				}
				else actualTrip = actualTrip.darSig();
			}
			
		}
		//System.out.println("numeroRoutes es "+numeroRoutes.get(0) );
		Node<VORoute> actualRoute = route.getCurrenteElement();
		//System.out.println("id de route "+ actualRoute.darData().id());
		boolean pepi = false;
		for (int i = 0; i < numeroRoutes.size(); i++) {
			while(actualRoute.darSig()!=null){
				//System.out.println("entro a for numer routes");
				if (actualRoute.darData().id() == (Integer)numeroRoutes.get(i)) {
					//System.out.println((Integer)numeroRoutes.get(i)+"="+actualRoute.darData().id());
					routeAtStop.add(actualRoute);
					pepi=true;
				}
				else actualRoute = actualRoute.darSig();
			}
			actualRoute=route.getElement(0);
			pepi = false;
		}
		return routeAtStop;
	}

	//@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		IList<VOStop> listasRoute = new RingList<>();
		Node<VORoute> current = route.getCurrenteElement();
		while(current.darSig() != null)
		{
			if(current.darData().routeShortName == routeName)
			{
				Node<VOTrip> currentT = current.darData().trips.getCurrenteElement();
				Node<VOStop> currentS = currentT.darData().stops.getCurrenteElement();
				while(currentS.darSig() != null)
				{
					listasRoute.add(currentS);
					currentS = currentS.darSig();
				}
			}
			current = current.darSig();
		}
		return listasRoute;
	}

}
