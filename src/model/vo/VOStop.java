package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop {
	
	public int stopID;
	public String stopCode;
	public String stopName;
	public String stopDesc;
	public String stopLat;
	public String stopLon;
	public String zoneID;
	public String stopURL;
	public String locationType;
	public String parentStation;
	
	public VOStop(int id1, String stopCode1, String name1, String Desc, String lat, String lon, String pZoneID, String URL, String locT, String parentS){
		stopID = id1;
		stopCode = stopCode1;
		stopName = name1;
		stopDesc = Desc;
		stopLat = lat;
		stopLon = lon;
		zoneID = pZoneID;
		stopURL = URL;
		locationType = locT;
		parentStation = parentS;
	}
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return stopID;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return stopName;
	}

}
