package model.vo;

public class VOStopTimes {

	public int tripID;
	public String arrivalTime;
	public String departureTime;
	public int stopID;
	public String stopSequence;
	public String stopHeadsign;
	public String pickupType;
	public String dropOffType;
	public String shapeDistTraveled;
	
	public VOStopTimes(int tripID1, String arrivalTime1, String departureTime1, int stopId1, String stopSequence1, String stopHeadsign1, String pickupType1, String dropOffType1, String shapeDistTraveled1) {
		tripID = tripID1;
		arrivalTime = arrivalTime1;
		departureTime = departureTime1;
		stopID = stopId1;
		stopSequence = stopSequence1;
		stopHeadsign = stopHeadsign1;
		pickupType = pickupType1;
		dropOffType = dropOffType1;
		shapeDistTraveled = shapeDistTraveled1;
	}
	public int darTripId(){
		return tripID;
	}
	public int darStopId(){
		return stopID;
	}
	
	
}