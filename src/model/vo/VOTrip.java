package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOTrip {
	public int roadID;
	public String serviceID;
	public int tripID;
	public String tripHead;
	public String tripShortName;
	public String directionID;
	public String blockID;
	public String shapeID;
	public String wheelchairAccessible;
	public String bikesAllowed;
	public DoubleLinkedList<VOStop> stops;
	
	public VOTrip(int pRoadID, String pServiceID, int pTripID, String head, String shortName, String pdirectionID, String pBlockID, String pShapeID, String pWheelchairAccessible, String pBikesAllowed){
		tripID = pTripID;
		roadID = pRoadID;
		serviceID = pServiceID;
		tripHead = head;
		tripShortName = shortName;
		directionID = pdirectionID;
		blockID = pBlockID;
		shapeID = pShapeID;
		wheelchairAccessible = pWheelchairAccessible;
		bikesAllowed = pBikesAllowed;
	}

	public int darTripId() {
		// TODO Auto-generated method stub
		return tripID;
	}

	public int darRoadId() {
		// TODO Auto-generated method stub
		return roadID;
	}
}
