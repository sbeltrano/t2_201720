package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a route object
 */
public class VORoute {
	
	public int routeID;
	public String agencyID;
	public String routeShortName;
	public String routeLongName;
	public String routeDesc;
	public String routeType;
	public String routeURL;
	public String routeColor;
	public String routeTextColor;
	public DoubleLinkedList<VOTrip> trips;
	
	public VORoute(int id, String agency, String shortName, String longName, String Desc, String type, String URL, String color, String textColor){
		routeID=id ;
		agencyID=agency ;
		routeShortName=shortName;
		routeLongName=longName  ;
		routeDesc=Desc;
		routeType=type;
		routeURL=URL ;
		routeColor=color  ;
		routeTextColor=textColor ;
	}
	/**
	 * @return id - Route's id number
	 */
	public int id() {
		// TODO Auto-generated method stub
		return routeID;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return routeLongName;
	}

}
